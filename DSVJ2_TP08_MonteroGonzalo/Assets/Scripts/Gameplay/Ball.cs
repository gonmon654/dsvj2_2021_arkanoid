﻿using UnityEngine;

public class Ball : MonoBehaviour
{
    [SerializeField] float initialSpeed;
    [SerializeField] Rigidbody ballRigidBody;
    private Vector3 currentDirection;

    private void Start()
    {
        currentDirection = transform.up;    
    }

    private void Update()
    {
        transform.position += (initialSpeed * currentDirection) * Time.deltaTime;
    }

    private void OnCollisionEnter(Collision collision)
    {
        currentDirection = Vector3.Reflect(currentDirection, collision.contacts[0].normal);
        if (collision.collider.CompareTag("Brick"))
        {
            Destroy(collision.gameObject);
        }
    }
}
