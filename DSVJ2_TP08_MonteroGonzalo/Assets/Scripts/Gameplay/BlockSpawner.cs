﻿using System.Collections.Generic;
using UnityEngine;

public class BlockSpawner : MonoBehaviour
{
    [SerializeField] int blockQuantityInX;
    [SerializeField] int blockQuantityInY;
    [SerializeField] GameObject block;

    private List<GameObject> bricks = new List<GameObject>();
    private float spacingX = 0.3f;
    private float spacingY = 1.0f;

    void Start()
    {
        Reset();
        for (int x = 0; x < blockQuantityInX; x++)
        {
            for (int y = 0; y < blockQuantityInY; y++)
            {
                Vector3 spawnPosition = new Vector3(transform.position.x + (block.transform.localScale.x + spacingX) * x, transform.position.y - (block.transform.localScale.y - spacingY) * y, transform.position.z);
                GameObject brick = Instantiate(block, spawnPosition, Quaternion.identity);
                bricks.Add(brick);
                brick.gameObject.transform.SetParent(this.gameObject.transform);
                GameManager.instance.IncreaseBlockCount();
            }
        }
    }
    private void Reset()
    {
        foreach (GameObject brick in bricks)
        {
            Destroy(brick);
        }
        bricks.Clear();
    }
}
