﻿using UnityEngine;

public class Bricks : MonoBehaviour
{
    [SerializeField] int pointsPerDestroy;

    private void Start()
    {
        GameManager.instance.IncreaseMaxScore(pointsPerDestroy);
    }

    private void OnDestroy()
    {
        if (!GameManager.instance.GetGameOver())
        {
            GameManager.instance.GainPoints(pointsPerDestroy);
            GameManager.instance.DecreaseBlockCount();
        }        
    }
}
