﻿using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] float speed;
    [SerializeField] Rigidbody playerRigidBody;

    private float input;

    void Update()
    {
        input = Input.GetAxisRaw("Horizontal");
    }

    private void FixedUpdate()
    {
        playerRigidBody.velocity = Vector3.right * input * speed;
    }
}
