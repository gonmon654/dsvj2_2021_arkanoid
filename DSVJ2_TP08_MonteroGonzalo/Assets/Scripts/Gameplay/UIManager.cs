﻿using UnityEngine;
using TMPro;

public class UIManager : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI score;
    [SerializeField] TextMeshProUGUI lives;

    private void Update()
    {
        score.text = "Score: " + GameManager.instance.GetPoints();
        lives.text = "Lives: " + GameManager.instance.GetLives();
    }
}
