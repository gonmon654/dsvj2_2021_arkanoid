﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BottomBlock : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Ball"))
        {
            Destroy(other.gameObject);
            GameManager.instance.LoseLives();
            GameManager.instance.BallDestroyed();
        }
    }
}
