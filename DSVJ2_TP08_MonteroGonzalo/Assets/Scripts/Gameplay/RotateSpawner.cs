﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateSpawner : MonoBehaviour
{
    [SerializeField] float minRange;
    [SerializeField] float maxRange;
    [SerializeField] GameObject ball;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && !GameManager.instance.GetBallState())
        {
            float randomRange = Random.Range(minRange, maxRange);
            transform.eulerAngles = new Vector3(0, 0, randomRange);
            Instantiate(ball, transform.position, transform.rotation);
        }
    }
}
