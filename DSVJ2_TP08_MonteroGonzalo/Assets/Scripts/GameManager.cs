﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    //Instance
    public static GameManager instance = null;

    //Player Variables
    private int lives;
    private int score;
    private int maxScore;
    private int blockCount;
    private bool victory;
    private bool ballExists;
    private bool gameOver;

    void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
            return;
        }
        instance = this;
        DontDestroyOnLoad(gameObject);
        ResetValues();
    }

    public void BallSpawn()
    {
        ballExists = true;
    }

    public void BallDestroyed()
    {
        ballExists = false;
    }

    public bool GetBallState()
    {
        return ballExists;
    }

    public void ResetValues()
    {
        lives = 3;
        score = 0;
        maxScore = 0;
        blockCount = 0;
        victory = false;
        ballExists = false;
    }

    public void GainPoints(int points)
    {
        score += points;
    }

    public int GetPoints()
    {
        return score;
    }

    public void LoseLives()
    {
        lives--;
        if (lives <= 0)
        {
            victory = false;
            gameOver = true;
            SwitchScene();
        }
    }

    public int GetLives()
    {
        return lives;
    }

    public void WinGame()
    {
        victory = true;
        gameOver = true;
        SwitchScene();
    }

    public void IncreaseBlockCount()
    {
        blockCount++;
    }

    public void DecreaseBlockCount()
    {
        blockCount--;
        if (blockCount <= 0)
        {         
            WinGame();
        }
    }

    public bool GetVictory()
    {
        return victory;
    }

    public void IncreaseMaxScore(int score)
    {
        maxScore += score;
    }

    public int GetMaxScore()
    {
        return maxScore;
    }

    public bool GetGameOver()
    {
        return gameOver;
    }

    private void SwitchScene()
    {
        SceneManager.LoadScene("Arkanoid End Game");
    }
}
