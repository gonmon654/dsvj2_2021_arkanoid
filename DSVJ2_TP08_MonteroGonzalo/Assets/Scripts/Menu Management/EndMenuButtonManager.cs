﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class EndMenuButtonManager : MonoBehaviour
{
    public void RestartGame()
    {
        GameManager.instance.ResetValues();
        SceneManager.LoadScene("Arkanoid In Game");
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
