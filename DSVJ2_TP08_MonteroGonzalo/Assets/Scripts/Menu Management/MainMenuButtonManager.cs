﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuButtonManager : MonoBehaviour
{
    public void EnterGame()
    {
        SceneManager.LoadScene("Arkanoid In Game");
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
