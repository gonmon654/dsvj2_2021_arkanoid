﻿using UnityEngine;
using UnityEngine.UI;

public class VictoryLossDecider : MonoBehaviour
{
    [SerializeField] RawImage victoryImage;
    [SerializeField] RawImage lossImage;
    [SerializeField] RawImage[] scoreSkulls;

    void Start()
    {
        if (GameManager.instance.GetVictory())
        {
            victoryImage.enabled = true;
        }
        else if (!GameManager.instance.GetVictory())
        {
            lossImage.enabled = true;
        }

        int thirdOfScore = GameManager.instance.GetMaxScore() / 3;
        scoreSkulls[0].enabled = true;
        
        if (GameManager.instance.GetPoints() >= thirdOfScore)
        {
            scoreSkulls[1].enabled = true;
        }
        if (GameManager.instance.GetPoints() >= thirdOfScore * 2)
        {
            scoreSkulls[2].enabled = true;
        }
    }
}
